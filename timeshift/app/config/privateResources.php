<?php

use Phalcon\Config;

return new Config([
    'privateResources' => [
        'admin' => [
            'index',
        ],
        'index' => [
            'index'
        ],
    ],
]);
