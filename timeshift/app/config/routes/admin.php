<?php

$router->add(
    '/admin',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'admin',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/auth',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'session',
        'action'     => 'auth',
    ]
);


$router->add(
    '/admin/users',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/users/create',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'create',
    ]
);

$router->add(
    '/admin/users/edit',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'edit',
    ]
);

$router->add(
    '/admin/users/search',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'search',
    ]
);

$router->add(
    '/admin/users/:id/remove',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/permissions',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'permissions',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/profiles',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'profiles',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/users/create',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'create',
    ]
);