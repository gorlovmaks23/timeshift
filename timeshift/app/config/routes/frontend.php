<?php

$router->add(
    '/',
    [
        'namespace'  => 'Timeshift\Controllers',
        'controller' => 'index',
        'action'     => 'index',
    ]
);