<?php

declare(strict_types=1);

namespace Timeshift\Controllers\Admin;

final class AdminController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
    }
}