<?php

declare(strict_types=1);

namespace Timeshift\Controllers\Admin;

use Phalcon\Mvc\Controller;
use Timeshift\Exception\AuthException;
use Timeshift\Forms\LoginForm;

final class SessionController extends Controller
{
    public function initialize()
    {
        $this->view->setLayoutsDir('admin/');
        $this->view->setTemplateBefore('session/auth');
    }

    public function authAction()
    {
        $form = new LoginForm();

        try {
            if (!$this->request->isPost()) {
                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }
            } else {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    $this->auth->check([
                        'email' => $this->request->getPost('email'),
                        'password' => $this->request->getPost('password'),
                        'remember' => $this->request->getPost('remember')
                    ]);
                    return $this->response->redirect('admin');
                }
            }
        } catch (AuthException $e) {
            $this->flash->error($e->getMessage());
        }

        $this->view->form = $form;
    }
}